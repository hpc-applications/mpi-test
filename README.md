# Small tests for MPI on Euler

## Hello world

In the directory, you can find a small hello world example which allows to check if a MPI version on the cluster is working properly.

## Singularity

This directory contains an example for running a container on the cluster with multiple nodes and MPI.
It relies on mpiBench to check the performances so you will need to pull it too (`git submodule init`).

The Makefile in this directory contains all the required commands.

1) Download slurm from the cluster `make download-slurm`
2) Create the image `make build`
3) Push it to a repository `REGISTRY=TODO make push`
4) [on Euler] Submit the job on Euler `make submit`
