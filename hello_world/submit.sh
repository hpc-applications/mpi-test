#!/bin/bash


#SBATCH -n 4 -x eu-a2p-277 -N 2
echo `hostname`


function test() {
    set -e
    module purge
    module load $1
    make clean || true
    make
    set +e
    srun mpi_hello_world > tmp 2>&1
    cat tmp | grep -q "rank 3 out of 4"
    SRUN=$?
    mpirun -n 4 $PWD/mpi_hello_world > tmp 2>&1
    # cat tmp
    cat tmp | grep -q "rank 3 out of 4"
    MPIRUN=$?

    if [ "$SRUN" == "0" ];
    then
        echo $1: srun is working
    else
        echo $1: srun FAILED
    fi
    if [ "$MPIRUN" == "1" ];
    then
        echo WARNING mpirun failed
    fi
    echo
}

MODULES=("gcc/6.3.0 openmpi/2.1.1" "gcc/8.2.0 openmpi/3.0.1" "gcc/9.3.0 openmpi/4.0.2" "gcc/8.2.0 openmpi/4.0.3" "gcc/9.3.0 openmpi/4.1.4")
for MOD in "${MODULES[@]}";
do
    export MPICC=mpicc
    test "$MOD"
done

export MPICC=mpiicc

MOD="intel/2022.1.2"
test "$MOD"

# get hosttype
hname=`hostname | cut -d "-" -f 2`

case $hname in

    g1 | a2p)
        # euler VI or VII node
        # set variables for Intel MPI here
	export FI_PROVIDER=verbs
        ;;
    *)
	export FI_PROVIDER=tcp
        # not Euler VI or VII
        ;;
esac
export I_MPI_PMI_LIBRARY=/cluster/apps/gcc-4.8.5/openmpi-4.1.4-pu2smponvdeu574nqolsw4rynnagngch/lib/libpmi.so


MOD="intel/2020.0"
test "$MOD"

MOD="intel/19.1.0"
test "$MOD"

MOD="intel/18.0.1"
test "$MOD"

MOD="intel/2018.1"
test "$MOD"
