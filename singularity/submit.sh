#!/bin/bash

#SBATCH -n 4 -N 2 --ntasks-per-node=2 -C ib
#SBATCH --time=00:05:00
#SBATCH --exclusive --contiguous

module load openmpi
make mpi-bench
mpirun -np 4 mpiBench/mpiBench
mpirun -np 4 singularity exec --bind /scratch:/scratch docker://registry.euler.hpc.ethz.ch/node-testing/mpi-bench:latest /data/mpiBench

# To check info from open mpi
# ompi_info
# singularity exec docker://registry.euler.hpc.ethz.ch/node-testing/mpi-bench:latest /opt/ompi/bin/ompi_info
