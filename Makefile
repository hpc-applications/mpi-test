.ONESHELL:
REGISTRY?=registry.euler.hpc.ethz.ch/node-testing

all: mpi-bench

build:
	docker build . -f singularity/Dockerfile -t mpi-bench:latest

push:
	docker tag mpi-bench:latest $(REGISTRY)/mpi-bench:latest
	docker push $(REGISTRY)/mpi-bench:latest

mpi-bench:
	cd mpiBench && make

download-slurm:
	scp -r euler.ethz.ch:/cluster/apps/slurm .

submit:
	sbatch singularity/submit.sh
